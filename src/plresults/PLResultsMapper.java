package plresults;

import java.io.IOException;
import java.util.*;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapreduce.*;

public class PLResultsMapper extends Mapper<LongWritable, Text, Text, Text> {
    private Text teamName = new Text();
    private Text values = new Text();

    public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        String line = value.toString();
        String[] tempValues = line.split(",");
        for (int i = 0; i < tempValues.length; i++){
            teamName.set(tempValues[0].concat(" - ".concat(tempValues[5])));
            values.set(tempValues[2].concat(",").concat(tempValues[3]).concat(",").concat(tempValues[4]));
        }
        context.write(teamName, values);

    }

}