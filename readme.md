# PLResults mapreduce
This map reduce program uses the Premier League dataset found in Kaggle: [Premier League DataSet](https://www.kaggle.com/zaeemnalla/premier-league/data).

### Content
In this repository you will find:
*  results.csv file to work with
*  intelliJ configuration files and project
*  part-r-00000 job output file 

Furthermore, you will find also the compiled java classes and the jar file for execution.

### Requirementes
Hadoop configured with Java8.


### Before execution
**Don't forget to upload in hdfs format the file results.csv to the path /hadoop/plresults/ (or other but modify execution command)**

Change your directory to `src/plresults/plresults`

### Command Line execution

First execution
`javac *.java -d . -cp $(hadoop classpath) ; jar cf plresults.jar plresults/*.class ; hadoop jar plresults.jar plresults.PLResultsDriver /hadoop/plresults/results.csv output/test`

Following ones
`rm -rf plresults ; rm plresults.jar ; hdfs dfs -rm -R output ; javac *.java -d . -cp $(hadoop classpath) ; jar cf plresults.jar plresults/*.class ; hadoop jar plresults.jar plresults.PLResultsDriver /hadoop/plresults/results.csv output/test`

### Check results

Click here to checking the [job history](http://localhost:19888) 

Click here to checking the [output](http://localhost:9870) 