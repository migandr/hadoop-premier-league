package plresults;

import org.apache.hadoop.fs.Path;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

public class PLResultsDriver {
    public static void main(String[] args) throws Exception{
        Job job = Job.getInstance();
        job.setJobName("PLResults");
        job.setJarByClass(PLResultsDriver.class);
        job.setMapperClass(PLResultsMapper.class);
        job.setReducerClass(PLResultsReducer.class);
        // job.setNumReduceTasks(0);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);
        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));
        System.exit(job.waitForCompletion(true)?0:1);
    }
}