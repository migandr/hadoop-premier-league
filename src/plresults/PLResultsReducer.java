package plresults;

import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class PLResultsReducer extends Reducer<Text, Text, Text, Text>{

    public void reduce(Text key, Iterable<Text> values, Context context)
            throws IOException, InterruptedException{
        int sum_home_score = 0;
        int sum_home_received = 0;
        int home_win = 0;
        int home_lose = 0;
        int draw = 0;
        for (Text goal : values){
            System.out.println(goal);
            String[] tempValues = goal.toString().split(",");
            sum_home_score += (int)Float.parseFloat(tempValues[0]);
            sum_home_received += (int)Float.parseFloat(tempValues[1]);
            switch (tempValues[2]){
                case "H":
                    home_win += 1;
                    break;
                case "A":
                    home_lose +=1;
                    break;
                case "D":
                    draw +=1;
                    break;
            }
        }
        String result = Integer.toString(sum_home_score).concat(", ")
                .concat(Integer.toString(sum_home_received)).concat(", ")
                .concat(Integer.toString(home_win)).concat(", ")
                .concat(Integer.toString(home_lose)).concat(", ")
                .concat(Integer.toString(draw));

        context.write(key, new Text(result));
    }

}